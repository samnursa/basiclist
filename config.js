const IMAGES = {
    growth: require('./assets/growth.png'),
    idea: require('./assets/idea.png'),
    loudspeaker: require('./assets/loudspeaker.png'),
    mindset: require('./assets/mindset.png'),
    motivated: require('./assets/motivated.png'),
    motivationalspeech: require('./assets/motivationalspeech.png'),
    muscle: require('./assets/muscle.png'),
    passion: require('./assets/passion.png'),
    performance: require('./assets/performance.png'),
    ribbon: require('./assets/ribbon.png'),
    skill: require('./assets/skill.png'),
    smile: require('./assets/smile.png'),
    success: require('./assets/success.png'),
    support: require('./assets/support.png'),
    talent: require('./assets/talent.png'),
    talent1: require('./assets/talent1.png'),
    target: require('./assets/target.png'),
    thumbup: require('./assets/thumbup.png'),
    top: require('./assets/top.png'),
    trophy: require('./assets/trophy.png')
  };
  
  export default IMAGES;