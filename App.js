/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, ScrollView, FlatList, Image, View, Dimensions} from 'react-native';
import IMAGES  from './config';

const WINDOW = Dimensions.get('window');
const widthByScreen = (size) => {
  return (size * WINDOW.width) / 100;
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        arrData: [],
        arrIcon: [
          {id:1,image:IMAGES.growth},
          {id:2,image:IMAGES.idea},
          {id:3,image:IMAGES.loudspeaker},
          {id:4,image:IMAGES.mindset},
          {id:5,image:IMAGES.motivated},
          {id:6,image:IMAGES.motivationalspeech},
          {id:7,image:IMAGES.muscle},
          {id:8,image:IMAGES.passion},
          {id:9,image:IMAGES.performance},
          {id:10,image:IMAGES.ribbon},
          {id:11,image:IMAGES.skill},
          {id:12,image:IMAGES.smile},
          {id:13,image:IMAGES.success},
          {id:14,image:IMAGES.support},
          {id:15,image:IMAGES.talent},
          {id:16,image:IMAGES.talent1},
          {id:17,image:IMAGES.target},
          {id:18,image:IMAGES.thumbup},
          {id:19,image:IMAGES.top},
          {id:20,image:IMAGES.trophy}
        ]
    };
  }

  componentWillMount() {
    this.loadData();
  }

  loadData() {
    fetch('https://api.jikan.moe/v3/manga/1/characters')
        .then(response => response.json())
        .then((data) => {
          this.setState({
            arrData: data.characters
          });
        }).catch((error) => {
            alert('Connection');
        })
  }

  rowItemImage(item) {
    return (
      <Image
        resizeMode='cover'
        style={styles.image}
        source={{uri: item.image_url}}
      />
    )
  }

  rowItemIcon(item) {
    return (
      <Image
        resizeMode='contain'
        style={styles.icon}
        source={item.image}
      />
    )
  }

  render() {
    const {arrData, arrIcon} = this.state;
    return (
      <ScrollView>
        <View style={styles.container}>
          <FlatList
            data={arrData}
            numColumns={2}
            renderItem={({ item }) => this.rowItemImage(item)}
            keyExtractor={item => item.mal_id}
          />
          <FlatList
            data={arrIcon}
            numColumns={5}
            renderItem={({ item }) => this.rowItemIcon(item)}
            keyExtractor={item => item.id}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: widthByScreen(50),
    height: 200,
  },
  icon:{
    width: widthByScreen(20),
    height: 50,
  }
});
